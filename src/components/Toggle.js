import React, {Component} from 'react';

export default class Toggle extends Component {
    constructor(props){
        super(props);
        this.state = {isToggleOn: true};

        this.handleClickBinded = this.handleClickBinded.bind(this);
    }

    handleClickBinded() {
        this.setState(state => ({
            isToggleOn: !this.state.isToggleOn
        }));
    }

    handleClickTroughEvent() {
        this.setState(state => ({
            isToggleOn: !this.state.isToggleOn
        }));
    }

    handleClickClassProperty = () => {
        this.setState(state => ({
            isToggleOn: !this.state.isToggleOn
        }));
    }

    render() {
        return(
            <div>
            <button onClick={this.handleClickBinded}>
                With Binding
            </button>
            <button onClick = {(e) => this.handleClickTroughEvent()}>
                With event creation
            </button>
            <button onClick={this.handleClickClassProperty}>
                With class property
            </button>

            <p>
                The state of the button: {this.state.isToggleOn ? 'ON':'OFF'}
            </p>
            </div>
        )
    }
}