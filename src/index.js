import React, { Component } from 'react';
import ComponentWithPropsAndState from './components/ComponentWithPropsAndState';
import { render } from 'react-dom';
import HellowWorls from './components/HellowWorld';
import Toggle from './components/Toggle';
import Input from './components/Input';


const SimpleComponentAsFunction = (props) => {
  return <p>
      This is a simple component too
    </p>
}

class App extends Component {
  constructor() {
    super();
    this.state = {
      name: "Jorge"
    };
  }

  render() {
    return (
      <div>
        <h1>Simple component</h1>
        <SimpleComponentAsFunction/>
        <h1>Component with props and state</h1>
        <ComponentWithPropsAndState name={this.state.name}/>
        <h1>REACT FUNCTIONS</h1>
        <HellowWorls/>
        <h1>Tipos de eventos Binding</h1>
        <Toggle/>
        <h1>INPUT</h1>
        <Input/>
      </div>
    );
  }
}

render(<App />, document.getElementById('root'));
